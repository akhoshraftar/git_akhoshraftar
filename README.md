# Memorial University - CMSC6950 - assignment 4
Memorial University of Newfoundland, also known as Memorial University or MUN, has a special obligation to the people of Newfoundland province. Established as a memorial to the Newfoundlanders who lost their lives on active service during the First World War and subsequent conflicts. MUN is a multicampus, multidisciplinary university dedicated to creativity, innovation, and excellence in teaching and learning, research, scholarship, and public engagement.

Memorial University offers a wide range of certificate, diploma, undergraduate, graduate and postgraduate programs, complemented by extensive online courses and degrees.

## Campuses
Memorial University has four main campuses and two satellite campuses across three regions of Newfoundland and Labrador, and in two countries (Canada and England).
